package com.playtech.live.combintation;

import static com.playtech.live.combintation.CombinationProbabilityCalculator.ACCURACY;
import static com.playtech.live.combintation.model.Card.C10;
import static com.playtech.live.combintation.model.Card.C9;
import static com.playtech.live.combintation.model.Card.CA;
import static com.playtech.live.combintation.model.Card.CJ;
import static com.playtech.live.combintation.model.Card.CK;
import static com.playtech.live.combintation.model.Card.CQ;
import static com.playtech.live.combintation.model.Card.D10;
import static com.playtech.live.combintation.model.Card.DJ;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.playtech.live.combintation.model.Card;
import com.playtech.live.combintation.model.Combination;

import org.junit.jupiter.api.Test;

import java.util.List;

class CombinationProbabilityCalculatorTest {

    private final CombinationProbabilityCalculator probabilityCalculator = createProbabilityCalculator();

    private CombinationProbabilityCalculator createProbabilityCalculator() {
        throw new RuntimeException("Provide your implementation here");
    }

    @Test
    void testNoRoyalFlushFromSixCards() {
        var royalFlushProbability = probabilityCalculator.probabilityOfCombination(Combination.ROYAL_FLUSH, List.of(CA, CK, CQ, DJ, D10, C9));
        assertEquals(0.0, royalFlushProbability, ACCURACY);
    }

    @Test
    void testRoyalFlushFromSixCards() {
        var royalFlushProbability = probabilityCalculator.probabilityOfCombination(Combination.ROYAL_FLUSH, List.of(CA, CK, CQ, CJ, C10, C9));
        assertEquals(0.1666667, royalFlushProbability, ACCURACY);
    }

    @Test
    void testRoyalFlushFromDeck() {
        var royalFlushProbability = probabilityCalculator.probabilityOfCombination(Combination.ROYAL_FLUSH, List.of(Card.values()));

        assertEquals(0.00000154, royalFlushProbability, ACCURACY);
    }

    @Test
    void testStraightFlushFromDeck() {
        var straightFlushProbability = probabilityCalculator.probabilityOfCombination(Combination.STRAIGHT_FLUSH, List.of(Card.values()));

        assertEquals(0.0000139, straightFlushProbability, ACCURACY);
    }

    @Test
    void testTwoPairsFromDeck() {
        var twoPairsProbability = probabilityCalculator.probabilityOfCombination(Combination.TWO_PAIRS, List.of(Card.values()));

        assertEquals(0.047539, twoPairsProbability, ACCURACY);
    }
}