package com.playtech.live.combintation.model;

public enum Combination {
    ROYAL_FLUSH,    // A, K, Q, J, 10, all the same suit. (CA, CK, CQ, CJ, C10)
    STRAIGHT_FLUSH, // Five cards in a sequence, all in the same suit. (CK, CQ, CJ, C10, C9)
    TWO_PAIRS,      // Two different pairs. (C7, D7, C8, C10, C10)
}
