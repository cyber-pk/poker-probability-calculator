package com.playtech.live.combintation;

import com.playtech.live.combintation.model.Card;
import com.playtech.live.combintation.model.Combination;

import java.util.List;

public interface CombinationProbabilityCalculator {

    double ACCURACY = 1e-7;

    int CARDS_COMBINATION_SIZE = 5;

    /**
     * Returns probability of a specific combination, if 5 cards for combination will be taken randomly from the deck
     * Result should be between 0 and 1.
     * Accuracy of your result should be less than value in {@link #ACCURACY}
     *
     * For example:
     * probability of royal flash for deck: [CA, CK, CQ, CJ, C10, C9] is equal to 0.16666666666667,
     * because in total we have 6 different combinations with 5 cards(order doesn't matter):
     * - [CA, CK, CQ, CJ, C10] - ROYAL_FLASH
     * - [CA, CK, CQ, CJ, C9]
     * - [CA, CK, CQ, C9, C10]
     * - [CA, CK, C9, CJ, C10]
     * - [CA, C9, CQ, CJ, C10]
     * - [C9, CK, CQ, CJ, C10],
     *
     * so, the answer is 1/6, as all combination count is 6, and accepted - 1
     *
     * Note:
     * - What if deck doesn't have enough cards?
     * - What if deck contains duplicate cards?
     * - Any other questions should be asked?
     *
     * **
     * - How this API can be improved, and what benefits it has?
     */
    double probabilityOfCombination(Combination combination, List<Card> deck);
}
